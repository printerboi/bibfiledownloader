import urllib.request, urllib.error, urllib.parse
import bibtexparser
import re
import os
import pdfkit
import mimetypes


donwloadfoldername = 'downloaded'
bibfilename = 'Literaturverzeichnis.bib'


failedToDownload = dict()

def downloadhtml(url, name):

    try:
        print(url)

        response = urllib.request.urlopen(url)
        webContent = response.read()
        ctt = response.headers['Content-Type']
        
        extension = mimetypes.guess_extension(ctt)
        print(extension)
        if extension == None:
            extension = ".html"
        

        f = open(donwloadfoldername + "/" + name + "/" + name + extension, 'wb')
        f.write(webContent)
        f.close
    except  urllib.error.HTTPError as err:
        failedToDownload[name]['html'] = True;


def download(url, name):

    try:
        print(url)
        pdfkit.from_url(url, donwloadfoldername + "/" + name + "/" + name + ".pdf")
    except:
        failedToDownload[name]['pdf'] = True;

   


def handleOutput(worked, nourl, failed):
    print("Found {} entries containing a link".format(len(worked)))
    if len(nourl) > 0:
        print("The following entries where declared as 'misc', but no url was provided...")
        for en in nourl:
            print("     => {}".format(en))

    if len(failed) > 0:
        print("The 'howpublished' value could not be parsed for the following entries...")
        for failedurlparse in failed:
            print("     => {}".format(failedurlparse))


def handleHotPublished(string):
    regex = r"(?<=\{)(.*?)(?=\})"
    matches = re.findall(regex, string, re.MULTILINE)
    if len(matches) == 1:
        return matches[0]
    else:
        return None


def parsebibtex(bibtexfile):
    library = bibtexparser.parse_file(bibtexfile)
    keysToDownload = list()
    keysNoUrl = list()
    entriesFailedToParse = list()

    for entry in library.entries:
        name = entry.key
        typeof = entry.entry_type

        if typeof == 'misc':
            fields = entry.fields_dict
            if 'url' in fields:
                keysToDownload.append(( name, fields['url'].value ))
            elif 'howpublished' in fields:
                link = handleHotPublished(fields['howpublished'].value)
                if link != None:
                    keysToDownload.append(( name, link ))
                else:
                    entriesFailedToParse.append(name)
            else:
                keysNoUrl.append(name)
                

    handleOutput(keysToDownload, keysNoUrl, entriesFailedToParse)

    if len(keysToDownload) > 0:
        isExist = os.path.exists(donwloadfoldername)
        if not isExist:
            os.makedirs(donwloadfoldername)

    for tup in keysToDownload:
        failedToDownload[tup[0]] = dict();
        failedToDownload[tup[0]]['pdf'] = False;
        failedToDownload[tup[0]]['html'] = False;

        entryfolder = donwloadfoldername + "/" + tup[0]

        isExist = os.path.exists(entryfolder)
        if not isExist:
            os.makedirs(entryfolder)

        download(tup[1], tup[0])
        downloadhtml(tup[1], tup[0])

    failedentries = list()
    for entr in failedToDownload.keys():
        for ty in failedToDownload[entr].keys():
            if failedToDownload[entr][ty]:
                failedentries.append((entr, ty, failedToDownload[entr][ty]))
    
    print("The download failed for the following {} entries...".format(len(failedToDownload)))
    
    for entr in failedentries:
        print("        {} => {} [{}]".format(entr[0], entr[1], entr[2]))
            

parsebibtex(bibfilename)
