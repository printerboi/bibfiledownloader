# Bibfiledownloader

Simple python script to parse a given `.bib` file that downloads every `misc` item that contains a `url` or `howpublished` key.
The page will be downloaded and saved in a given subfolder (defaults to `downloaded/`).

### Install

To install install the python-bibtexparser v2 package. Please refer to their repository for further advice
https://github.com/sciunto-org/python-bibtexparser.


### How to run

Place the `.bib` you want to download in the same folder as the `downloader.py` script and execute it.